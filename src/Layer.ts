import { Option } from "@aicacia/core";
import { EventEmitter } from "events";
import { format } from "util";
import { IContext, IParams } from "./context";
import { IMiddleware } from "./IMiddleware";
import { buildPath } from "./utils/buildPath";
import { cleanPath } from "./utils/cleanPath";
import { filterParams } from "./utils/filterParams";
import { LayerAsMiddlewareError } from "./utils/LayerAsMiddlewareError";
import { formatPath, Param, pathToMatcher } from "./utils/pathToMatcher";

export class Layer extends EventEmitter implements IMiddleware {
  protected parent: Option<Layer>;
  protected end: boolean;
  protected relativePath = "";
  protected path = "";
  protected formatPath = "";
  protected params: Param[] = [];
  protected regExp: RegExp;
  protected meta: Record<string, any>;

  constructor(
    path = "/",
    parent: Option<Layer>,
    end = true,
    meta: Record<string, any> = {}
  ) {
    super();

    this.parent = parent;
    this.end = end;
    this.meta = meta;
    this.regExp = this.createRegExp(path);
  }

  getMeta() {
    return this.meta;
  }
  setMeta(meta: Record<string, any>): ThisType<this> {
    this.meta = meta;
    return this;
  }

  getParent() {
    return this.parent;
  }
  getRoot(): ThisType<this> {
    return this.getParent()
      .map((parent) => parent.getRoot())
      .unwrapOr(this);
  }

  middleware(_error: Error | null, _context: IContext): Promise<IContext> {
    return Promise.reject(new LayerAsMiddlewareError());
  }

  getFormatPath(): string {
    return this.formatPath;
  }

  getPath(): string {
    return this.path;
  }

  getRegExp(): RegExp {
    return this.regExp;
  }

  isEnd(): boolean {
    return this.end;
  }

  match(path: string): IParams | false {
    return filterParams(this.regExp, this.params, path);
  }

  createRegExp(path: string): RegExp {
    this.relativePath = cleanPath(path);
    this.path = buildPath(this.parent, this.relativePath);
    this.formatPath = formatPath(this.path);
    const { regExp, params } = pathToMatcher(this.path, this.end);
    this.regExp = regExp;
    this.params = params;
    return regExp;
  }

  setPath(path: string): Layer {
    this.createRegExp(path);
    return this;
  }
  recompilePath(): Layer {
    return this.setPath(this.relativePath);
  }

  format(...args: any[]) {
    return format(this.formatPath, ...args);
  }
}
