import { IHandler } from "./IHandler";

export interface IMiddleware {
  middleware: IHandler;
}
