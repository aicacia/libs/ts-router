import { none, Option, some } from "@aicacia/core";
import { isFunction, isString, isObject } from "util";
import { IContext } from "./context";
import { IHandler } from "./IHandler";
import { IMiddleware } from "./IMiddleware";
import { Layer } from "./Layer";
import { Middleware } from "./Middleware";
import { Route } from "./Route";
import { CanceledError } from "./utils/CanceledError";
import { promisifyResult } from "./utils/promisifyResult";
import { UnhandledError } from "./utils/UnhandledError";

export class Router extends Layer {
  Middleware: new (
    path?: string,
    parent?: Option<Layer>,
    end?: boolean,
    meta?: Record<string, any>
  ) => Middleware;
  Route: new (
    path?: string,
    parent?: Option<Layer>,
    meta?: Record<string, any>
  ) => Route;
  Router: new (
    path?: string,
    parent?: Option<Layer>,
    meta?: Record<string, any>
  ) => Router;
  protected layers: Layer[];

  constructor(
    path = "/",
    parent: Option<Layer> = none(),
    meta: Record<string, any> = {}
  ) {
    super(path, parent, false, meta);

    this.Middleware = Middleware;
    this.Route = Route;
    this.Router = Router;

    this.layers = [];
  }

  middleware(error: Error | null, context: IContext): Promise<IContext> {
    const resolveSuccess = (): Promise<IContext> =>
      context.resolved()
        ? Promise.reject(new CanceledError())
        : Promise.resolve(context);

    const resolveError = (error: Error): Promise<IContext> =>
      error && error.message === CanceledError.MESSAGE
        ? Promise.resolve(context)
        : Promise.reject(error);

    return this.layers
      .reduce(
        (promise, layer) => {
          const params = layer.match(context.pathname);

          if (params) {
            context.params = {
              ...context.params,
              ...params,
            };

            if (layer instanceof this.Router) {
              context.route = context.middleware = undefined;
              context.router = layer as Router;
            } else if (layer instanceof this.Route) {
              context.router = context.middleware = undefined;
              context.route = layer as Route;
            } else {
              context.router = context.route = undefined;
              context.middleware = layer as Middleware;
            }
            context.layer = layer;

            return promise
              .then(
                () => promisifyResult(context, layer.middleware(null, context)),
                (error) =>
                  error && error.message === CanceledError.MESSAGE
                    ? Promise.reject(error)
                    : promisifyResult(context, layer.middleware(error, context))
              )
              .then(resolveSuccess);
          } else {
            return promise;
          }
        },
        error ? Promise.reject(error) : Promise.resolve(context)
      )
      .then(() => {
        if (!context.resolved()) {
          return Promise.reject(new UnhandledError());
        } else {
          return context;
        }
      }, resolveError);
  }

  handle(context: IContext): Promise<IContext> {
    const params = this.match(context.pathname);

    if (params) {
      context.params = { ...context.params, ...params };
      return this.middleware(null, context);
    } else {
      return Promise.reject(new UnhandledError());
    }
  }

  setPath(path: string): Router {
    super.setPath(path);

    this.layers.forEach((layer) => layer.recompilePath());

    return this;
  }

  recompilePath(): Router {
    super.recompilePath();

    this.layers.forEach((layer) => layer.recompilePath());

    return this;
  }

  use(
    path: string,
    meta: Record<string, any>,
    ...handlers: Array<IHandler | IMiddleware>
  ): Middleware;
  use(path: string, ...handlers: Array<IHandler | IMiddleware>): Middleware;
  use(
    meta: Record<string, any>,
    ...handlers: Array<IHandler | IMiddleware>
  ): Middleware;
  use(...handlers: Array<IHandler | IMiddleware>): Middleware;

  use(...args: any[]): Middleware {
    let path = "",
      meta: Record<string, any> = {},
      handlers: Array<IHandler | IMiddleware> = [];

    if (isString(args[0]) && isObject(args[1])) {
      path = args[0];
      meta = args[1];
      handlers = args.slice(2);
    }
    if (isString(args[0]) && isFunction(args[1])) {
      path = args[0];
      handlers = args.slice(1);
    }
    if (isObject(args[0]) && isFunction(args[1])) {
      meta = args[0];
      handlers = args.slice(1);
    }
    const middleware = new this.Middleware(path, some(this), false, meta).mount(
      ...handlers
    );
    this.layers.push(middleware);
    return middleware;
  }

  route(
    path: string,
    meta: Record<string, any>,
    ...handlers: Array<IHandler | IMiddleware>
  ): Route;
  route(path: string, ...handlers: Array<IHandler | IMiddleware>): Middleware;
  route(
    meta: Record<string, any>,
    ...handlers: Array<IHandler | IMiddleware>
  ): Route;
  route(...handlers: Array<IHandler | IMiddleware>): Route;

  route(...args: any[]): Route {
    let path = "",
      meta: Record<string, any> = {},
      handlers: Array<IHandler | IMiddleware> = [];

    if (isString(args[0]) && isObject(args[1])) {
      path = args[0];
      meta = args[1];
      handlers = args.slice(2);
    }
    if (isString(args[0]) && isFunction(args[1])) {
      path = args[0];
      handlers = args.slice(1);
    }
    if (isObject(args[0]) && isFunction(args[1])) {
      meta = args[0];
      handlers = args.slice(1);
    }
    const route = new this.Route(path, some(this), meta).mount(...handlers);
    this.layers.push(route);
    return route;
  }

  scope(path = "/", meta: Record<string, any> = {}): Router {
    const router = new this.Router(path, some(this), meta);
    this.layers.push(router);
    return router;
  }

  mount(path = "/", router: Router): Router {
    this.use(path, router);
    return router;
  }

  clear(): Router {
    this.layers.length = 0;
    return this;
  }
}
