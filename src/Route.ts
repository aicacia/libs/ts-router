import { none, Option } from "@aicacia/core";
import { Layer } from "./Layer";
import { Middleware } from "./Middleware";

export class Route extends Middleware {
  constructor(
    path = "/",
    parent: Option<Layer> = none(),
    meta: Record<string, any> = {}
  ) {
    super(path, parent, true, meta);
  }
}
