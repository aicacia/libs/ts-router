import { none, Option } from "@aicacia/core";
import { IContext } from "./context";
import { IHandler } from "./IHandler";
import { IMiddleware } from "./IMiddleware";
import { Layer } from "./Layer";
import { CanceledError } from "./utils/CanceledError";
import { createErrorHandler } from "./utils/createErrorHandler";

export type IMiddlewareHandler = (
  error: Error | null,
  context: IContext
) => Promise<IContext>;

export class Middleware extends Layer {
  protected handlers: IMiddlewareHandler[];

  constructor(
    path = "/",
    parent: Option<Layer> = none(),
    end = false,
    meta: Record<string, any> = {}
  ) {
    super(path, parent, end, meta);

    this.handlers = [];
  }

  middleware(error: Error | null, context: IContext): Promise<IContext> {
    return this.handlers
      .reduce(
        (promise, handler) =>
          promise.then(
            () => handler(error, context),
            (error) =>
              error && error.message === CanceledError.MESSAGE
                ? Promise.reject(error)
                : handler(error, context)
          ),
        error ? Promise.reject(error) : Promise.resolve(context)
      )
      .then((context) => {
        if (this.isEnd()) {
          context.end();
        }
        return context;
      });
  }

  mount(...handlers: (IHandler | IMiddleware)[]): Middleware {
    handlers.reduce((handlers, handler) => {
      handlers.push(createErrorHandler(handler));
      return handlers;
    }, this.handlers);
    return this;
  }

  clear(): Middleware {
    this.handlers.length = 0;
    return this;
  }
}
