import { IContext } from "./context";

export type IErrorHandler = (
  error: Error | null,
  context: IContext
) => Promise<IContext> | IContext | void;

export type IContextHandler = (
  context: IContext
) => Promise<IContext> | IContext | void;

export type IEmptyHandler = () => Promise<IContext> | IContext | void;

export type IHandler = IContextHandler | IErrorHandler | IEmptyHandler;
