export class CanceledError extends Error {
  static MESSAGE = "cancelled";

  constructor() {
    super(CanceledError.MESSAGE);
  }
}
