import { IParams } from "../context";
import { Param } from "./pathToMatcher";

export const filterParams = (
  regExp: RegExp,
  params: Param[],
  path: string
): IParams | false => {
  const results: string[] = regExp.exec(path) || [];

  if (!results || results.length === 0) {
    return false;
  } else {
    results.shift(); // remove full match from results

    return results.reduce(
      (filteredParams: IParams, result: string, index: number) => {
        const param = params[index];

        if (param != null) {
          filteredParams[param.name] = decodeURIComponent(result);
        }

        return filteredParams;
      },
      {}
    );
  }
};
