import * as tape from "tape";
import { isBoolean } from "util";
import { pathToMatcher } from "./pathToMatcher";

function testPath(
  assert: tape.Test,
  path: string,
  matches: Array<[string, string[] | boolean]>,
  end = true
) {
  const regExp = pathToMatcher(path, end).regExp;

  matches.forEach(([url, params]) => {
    if (isBoolean(params)) {
      assert.deepEquals(url.match(regExp), null);
    } else {
      assert.deepEquals(url.match(regExp)?.slice(1), params);
    }
  });
}

tape(
  "pathToMatcher(path: string, end: boolean = true)",
  (assert: tape.Test) => {
    testPath(assert, "/parent/:parent_id/child/:child_id", [
      ["/parent/1/child/2", ["1", "2"]],
      ["/parent/adf8awfolf/child/asdflasndfaf", ["adf8awfolf", "asdflasndfaf"]],
    ]);
    testPath(assert, "/parent/:parent_id{\\d+}", [
      ["/parent/1", ["1"]],
      ["/parent/asdf", false],
    ]);
    testPath(assert, "/parent/:rest{.*}", [
      ["/parent/1/child/2", ["1/child/2"]],
    ]);
    testPath(assert, "/parent(/:hello)", [["/parent/hello", ["hello"]]]);
    assert.end();
  }
);
