import { Option } from "@aicacia/core";
import { Layer } from "../Layer";

export const buildPath = (parent: Option<Layer>, path?: string) => {
  if (!path) {
    if (parent.isNone()) {
      return "/";
    } else {
      return parent.unwrap().getPath();
    }
  } else {
    if (path.charAt(0) !== "/") {
      path = "/" + path;
    }
    if (path.charAt(path.length - 1) === "/") {
      path = path.slice(0, -1);
    }

    if (parent.isSome()) {
      path = parent.unwrap().getPath() + path;
    }

    return path;
  }
};
