export class UnhandledError extends Error {
  static MESSAGE = "unhandled";

  constructor() {
    super(UnhandledError.MESSAGE);
  }
}
