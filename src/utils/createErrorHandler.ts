import { IContext } from "../context";
import { IContextHandler, IErrorHandler, IHandler } from "../IHandler";
import { IMiddleware } from "../IMiddleware";
import { IMiddlewareHandler } from "../Middleware";
import { promisifyResult } from "./promisifyResult";

export const createErrorHandler = (
  handler: IHandler | IMiddleware
): IMiddlewareHandler => {
  if (typeof handler === "object") {
    const middleware = handler as IMiddleware;

    if (middleware.middleware.length >= 2) {
      return (error: Error | null, context: IContext) =>
        promisifyResult(
          context,
          (middleware.middleware as IErrorHandler)(error, context)
        );
    } else {
      return (error: Error | null, context: IContext) =>
        error
          ? Promise.reject(error)
          : promisifyResult(
              context,
              (middleware.middleware as IContextHandler)(context)
            );
    }
  } else if (typeof handler === "function") {
    if (handler.length >= 2) {
      return (error: Error | null, context: IContext) =>
        promisifyResult(context, (handler as IErrorHandler)(error, context));
    } else {
      return (error: Error | null, context: IContext) =>
        error
          ? Promise.reject(error)
          : promisifyResult(context, (handler as IContextHandler)(context));
    }
  } else {
    throw new TypeError("Invalid Handler " + handler);
  }
};
